package org.cleverframe.fastdfs.thumb;

import org.apache.commons.lang3.Validate;
import org.cleverframe.fastdfs.thumb.ThumbImageConfig;

/**
 * 缩略图配置参数
 * 
 * @author tobato
 *
 */
public class DefaultThumbImageConfig implements ThumbImageConfig {
	
	private int width=150;

    private int height=150;

    private static String cachedPrefixName;

    /**
     * 生成前缀如:_150x150
     */
    @Override
    public String getPrefixName(Integer width,Integer height) {
    	if(width==null){
    		width=this.width;
    	}
    	if(height==null){
    		height=this.height;
    	}
        if (cachedPrefixName == null) {
            StringBuilder buffer = new StringBuilder();
            buffer.append("_").append(width).append("x").append(height);
            cachedPrefixName = new String(buffer);
        }
        return cachedPrefixName;
    }

    /**
     * 根据文件名获取缩略图路径
     */
    @Override
    public String getThumbImagePath(String masterFilename,Integer width,Integer height) {
    	Validate.notBlank(masterFilename, "主文件不能为空");
        StringBuilder buff = new StringBuilder(masterFilename);
        int index = buff.lastIndexOf(".");
        buff.insert(index, getPrefixName(width,height));
        return buff.toString();
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


}

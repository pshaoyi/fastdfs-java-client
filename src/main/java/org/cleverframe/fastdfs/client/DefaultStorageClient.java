package org.cleverframe.fastdfs.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.cleverframe.fastdfs.conn.CommandExecutor;
import org.cleverframe.fastdfs.constant.ErrorCodeConstants;
import org.cleverframe.fastdfs.exception.FastDfsServerException;
import org.cleverframe.fastdfs.exception.FdfsUnsupportImageTypeException;
import org.cleverframe.fastdfs.exception.FdfsUploadImageException;
import org.cleverframe.fastdfs.model.FileInfo;
import org.cleverframe.fastdfs.model.MateData;
import org.cleverframe.fastdfs.model.StorageNode;
import org.cleverframe.fastdfs.model.StorageNodeInfo;
import org.cleverframe.fastdfs.model.StorePath;
import org.cleverframe.fastdfs.model.ThumbImage;
import org.cleverframe.fastdfs.protocol.storage.AppendFileCommand;
import org.cleverframe.fastdfs.protocol.storage.DeleteFileCommand;
import org.cleverframe.fastdfs.protocol.storage.DownloadFileCommand;
import org.cleverframe.fastdfs.protocol.storage.GetMetadataCommand;
import org.cleverframe.fastdfs.protocol.storage.ModifyCommand;
import org.cleverframe.fastdfs.protocol.storage.QueryFileInfoCommand;
import org.cleverframe.fastdfs.protocol.storage.SetMetadataCommand;
import org.cleverframe.fastdfs.protocol.storage.TruncateCommand;
import org.cleverframe.fastdfs.protocol.storage.UploadFileCommand;
import org.cleverframe.fastdfs.protocol.storage.UploadSlaveFileCommand;
import org.cleverframe.fastdfs.protocol.storage.callback.DownloadCallback;
import org.cleverframe.fastdfs.protocol.storage.enums.StorageMetadataSetType;
import org.cleverframe.fastdfs.thumb.ThumbImageConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 存储服务(Storage)客户端接口 默认实现<br/>
 * <b>注意: 当前类最好使用单例，一个应用只需要一个实例</b> 作者：LiZW <br/>
 * 创建时间：2016/11/21 16:25 <br/>
 */
public class DefaultStorageClient implements StorageClient {

	/**
	 * 日志
	 */
	private static final Logger logger = LoggerFactory.getLogger(DefaultStorageClient.class);

	private CommandExecutor commandExecutor;

	private TrackerClient trackerClient;

	/** 缩略图生成配置 */
	private ThumbImageConfig thumbImageConfig;

	/** 支持的图片类型 */
	private static final String[] SUPPORT_IMAGE_TYPE = { "JPG", "JPEG", "PNG", "GIF", "BMP", "WBMP" };
	private static final List<String> SUPPORT_IMAGE_LIST = Arrays.asList(SUPPORT_IMAGE_TYPE);

	public DefaultStorageClient(CommandExecutor commandExecutor, TrackerClient trackerClient) {
		this.commandExecutor = commandExecutor;
		this.trackerClient = trackerClient;
	}

	public DefaultStorageClient(CommandExecutor commandExecutor, TrackerClient trackerClient,
			ThumbImageConfig thumbImageConfig) {
		this.commandExecutor = commandExecutor;
		this.trackerClient = trackerClient;
		this.thumbImageConfig = thumbImageConfig;
	}

	@Override
	public StorePath uploadFile(String groupName, InputStream inputStream, long fileSize, String fileExtName) {
		StorageNode storageNode = trackerClient.getStorageNode(groupName);
		UploadFileCommand command = new UploadFileCommand(storageNode.getStoreIndex(), inputStream, fileExtName,
				fileSize, false);
		return commandExecutor.execute(storageNode.getInetSocketAddress(), command);
	}

	@Override
	public StorePath uploadSlaveFile(String groupName, String masterFilename, InputStream inputStream, long fileSize,
			String prefixName, String fileExtName) {
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, masterFilename);
		UploadSlaveFileCommand command = new UploadSlaveFileCommand(inputStream, fileSize, masterFilename, prefixName,
				fileExtName);
		return commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
	}

	@Override
	public Set<MateData> getMetadata(String groupName, String path) {
		try {
			StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorage(groupName, path);
			GetMetadataCommand command = new GetMetadataCommand(groupName, path);
			return commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
		} catch (Throwable e) {
			logger.error("获取文件元信息", e);
			return new HashSet<MateData>();
		}
	}

	@Override
	public boolean overwriteMetadata(String groupName, String path, Set<MateData> metaDataSet) {
		try {
			StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
			SetMetadataCommand command = new SetMetadataCommand(groupName, path, metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
			commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
		} catch (Throwable e) {
			logger.error("修改文件元信息（覆盖）失败", e);
			return false;
		}
		return true;
	}

	@Override
	public boolean mergeMetadata(String groupName, String path, Set<MateData> metaDataSet) {
		try {
			StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
			SetMetadataCommand command = new SetMetadataCommand(groupName, path, metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_MERGE);
			commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
		} catch (Throwable e) {
			logger.error("修改文件元信息（合并）失败", e);
			return false;
		}
		return true;
	}

	@Override
	public FileInfo queryFileInfo(String groupName, String path) {
		FileInfo fileInfo = null;
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorage(groupName, path);
		QueryFileInfoCommand command = new QueryFileInfoCommand(groupName, path);
		try {
			fileInfo = commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
		} catch (FastDfsServerException e) {
			if (e.getErrorCode() == ErrorCodeConstants.ERR_NO_ENOENT) {
				logger.warn("获取文件的信息异常,ErrorCode=[{}], ErrorMessage=[{}]", e.getErrorCode(), e.getMessage());
			} else {
				throw e;
			}
		}
		return fileInfo;
	}

	@Override
	public boolean deleteFile(String groupName, String path) {
		try {
			StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
			DeleteFileCommand command = new DeleteFileCommand(groupName, path);
			commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
		} catch (Throwable e) {
			logger.error("删除文件失败", e);
			return false;
		}
		return true;
	}

	@Override
	public boolean deleteFile(String filePath) {
		StorePath storePath = StorePath.parseFromUrl(filePath);
		return deleteFile(storePath.getGroup(), storePath.getPath());
	}

	@Override
	public <T> T downloadFile(String groupName, String path, DownloadCallback<T> callback) {
		long fileOffset = 0;
		long fileSize = 0;
		return downloadFile(groupName, path, fileOffset, fileSize, callback);
	}

	@Override
	public <T> T downloadFile(String groupName, String path, long fileOffset, long fileSize,
			DownloadCallback<T> callback) {
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorage(groupName, path);
		DownloadFileCommand<T> command = new DownloadFileCommand<T>(groupName, path, fileOffset, fileSize, callback);
		return commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public StorePath uploadFile(InputStream inputStream, long fileSize, String fileExtName, Set<MateData> metaDataSet) {
		StorageNode storageNode = trackerClient.getStorageNode();
		UploadFileCommand command = new UploadFileCommand(storageNode.getStoreIndex(), inputStream, fileExtName,
				fileSize, false);
		StorePath storePath = commandExecutor.execute(storageNode.getInetSocketAddress(), command);
		if (hasMateData(metaDataSet)) {
			// 参数存储
			SetMetadataCommand cmd = new SetMetadataCommand(storePath.getGroup(), storePath.getPath(), metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
			commandExecutor.execute(storageNode.getInetSocketAddress(), cmd);
		}
		return storePath;
	}

	@Override
	public StorePath uploadFile(InputStream inputStream, byte storeIndex, long fileSize, String fileExtName,
			Set<MateData> metaDataSet) {
		StorageNode storageNode = trackerClient.getStorageNode();
		UploadFileCommand command = new UploadFileCommand(storeIndex, inputStream, fileExtName, fileSize, false);
		StorePath storePath = commandExecutor.execute(storageNode.getInetSocketAddress(), command);
		if (hasMateData(metaDataSet)) {
			// 参数存储
			SetMetadataCommand cmd = new SetMetadataCommand(storePath.getGroup(), storePath.getPath(), metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
			commandExecutor.execute(storageNode.getInetSocketAddress(), cmd);
		}
		return storePath;
	}

	@Override
	public StorePath uploadAppenderFile(String groupName, InputStream inputStream, long fileSize, String fileExtName) {
		StorageNode storageNode = trackerClient.getStorageNode(groupName);
		UploadFileCommand command = new UploadFileCommand(storageNode.getStoreIndex(), inputStream, fileExtName,
				fileSize, true);
		return commandExecutor.execute(storageNode.getInetSocketAddress(), command);
	}

	@Override
	public void appendFile(String groupName, String path, InputStream inputStream, long fileSize) {
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
		AppendFileCommand command = new AppendFileCommand(inputStream, fileSize, path);
		commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
	}

	@Override
	public void modifyFile(String groupName, String path, InputStream inputStream, long fileSize, long fileOffset) {
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
		ModifyCommand command = new ModifyCommand(path, inputStream, fileSize, fileOffset);
		commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
	}

	@Override
	public void truncateFile(String groupName, String path, long truncatedFileSize) {
		StorageNodeInfo storageNodeInfo = trackerClient.getFetchStorageAndUpdate(groupName, path);
		TruncateCommand command = new TruncateCommand(path, truncatedFileSize);
		commandExecutor.execute(storageNodeInfo.getInetSocketAddress(), command);
	}

	@Override
	public void truncateFile(String groupName, String path) {
		long truncatedFileSize = 0;
		truncateFile(groupName, path, truncatedFileSize);
	}

	/**
	 * 上传图片并且生成缩略图
	 */
	@Override
	public StorePath uploadImageAndCrtThumbImage(InputStream inputStream, long fileSize, String fileExtName,
			Set<MateData> metaDataSet, Integer width, Integer height) {
		Validate.notBlank(fileExtName, "文件扩展名不能为空");
		Validate.notNull(inputStream, "上传文件流不能为空");
		// 检查是否能处理此类图片
		if (!isSupportImage(fileExtName)) {
			throw new FdfsUnsupportImageTypeException("不支持的图片格式" + fileExtName);
		}
		StorageNode client = trackerClient.getStorageNode();
		return uploadImageAndThumbImage(client, inputStream, client.getStoreIndex(), fileSize, fileExtName, metaDataSet, width, height);
	}
	
	@Override
	public StorePath uploadImageAndCrtThumbImage(InputStream inputStream,byte storeIndex, long fileSize, String fileExtName,
			Set<MateData> metaDataSet, Integer width, Integer height) {
		Validate.notBlank(fileExtName, "文件扩展名不能为空");
		Validate.notNull(inputStream, "上传文件流不能为空");
		// 检查是否能处理此类图片
		if (!isSupportImage(fileExtName)) {
			throw new FdfsUnsupportImageTypeException("不支持的图片格式" + fileExtName);
		}
		StorageNode client = trackerClient.getStorageNode();
		return uploadImageAndThumbImage(client, inputStream, storeIndex, fileSize, fileExtName, metaDataSet, width, height);
	}
	
	/**
	 * 
	 * @param node
	 * @param inputStream
	 * @param storeIndex
	 * @param fileSize
	 * @param fileExtName
	 * @param metaDataSet
	 * @param width
	 * @param height
	 * @return
	 */
	public StorePath uploadImageAndThumbImage(StorageNode node,InputStream inputStream,byte storeIndex, long fileSize, String fileExtName,
			Set<MateData> metaDataSet, Integer width, Integer height) {
		byte[] bytes = inputStreamToByte(inputStream);
		// 上传文件和mateData
		StorePath path = uploadFileAndMateData(node, new ByteArrayInputStream(bytes),storeIndex, fileSize, fileExtName,metaDataSet);
		// 上传缩略图
		StorePath thumbPath = uploadThumbImage(node, new ByteArrayInputStream(bytes),storeIndex, path.getPath(), fileExtName,
				width, height);
		if (thumbPath != null) {
			// 放置缩略图
			path.setThumbPath(thumbPath);
		}
		bytes = null;
		return path;
	}

	/**
	 * 上传文件和元数据
	 * @param client
	 * @param inputStream
	 * @param fileSize
	 * @param fileExtName
	 * @param metaDataSet
	 * @return
	 */
	private StorePath uploadFileAndMateData(StorageNode storageNode, InputStream inputStream, long fileSize,
			String fileExtName, Set<MateData> metaDataSet) {
		return uploadFileAndMateData(storageNode, inputStream,storageNode.getStoreIndex(),fileSize, fileExtName, metaDataSet);
	}
	
	/**
	 * 上传文件和元数据
	 * @param storageNode
	 * @param inputStream
	 * @param storeIndex
	 * @param fileSize
	 * @param fileExtName
	 * @param metaDataSet
	 * @return
	 */
	private StorePath uploadFileAndMateData(StorageNode storageNode, InputStream inputStream,byte storeIndex, long fileSize,
			String fileExtName, Set<MateData> metaDataSet) {
		// 上传文件
		UploadFileCommand command = new UploadFileCommand(storeIndex, inputStream, fileExtName,fileSize, false);
		StorePath storePath = commandExecutor.execute(storageNode.getInetSocketAddress(), command);
		// 上传matadata
		if (hasMateData(metaDataSet)) {
			// 参数存储
			SetMetadataCommand cmd = new SetMetadataCommand(storePath.getGroup(), storePath.getPath(), metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
			commandExecutor.execute(storageNode.getInetSocketAddress(), cmd);
		}
		return storePath;
	}

	/**
	 * 上传缩略图
	 * @param client
	 * @param inputStream
	 * @param fileSize
	 * @param fileExtName
	 * @param metaDataSet
	 */
	private StorePath uploadThumbImage(StorageNode storageNode, InputStream inputStream, String masterFilename,
			String fileExtName, Integer width, Integer height) {
		return uploadThumbImage(storageNode, inputStream, storageNode.getStoreIndex(), masterFilename, fileExtName, width, height);
	}
	
	/**
	 * 上传缩略图
	 * @param storageNode
	 * @param inputStream
	 * @param storeIndex
	 * @param masterFilename
	 * @param fileExtName
	 * @param width
	 * @param height
	 * @return
	 */
	private StorePath uploadThumbImage(StorageNode storageNode, InputStream inputStream,byte storeIndex, String masterFilename,
			String fileExtName, Integer width, Integer height) {
		ByteArrayInputStream thumbImageStream = null;
		try {
			thumbImageStream = getThumbImageStream(inputStream, width, height);// getFileInputStream
			generateThumbImageByDefault(inputStream);// getFileInputStream
			// 获取文件大小
			long fileSize = thumbImageStream.available();
			// 获取缩略图前缀
			String prefixName = thumbImageConfig.getPrefixName(width, height);
			UploadSlaveFileCommand command = new UploadSlaveFileCommand(thumbImageStream, fileSize, masterFilename,prefixName, fileExtName);
			StorePath storePath = commandExecutor.execute(storageNode.getInetSocketAddress(), command);
			logger.info("缩略图路径：" + storePath.getFullPath());
			return storePath;
		} catch (IOException e) {
			logger.error("upload ThumbImage error", e);
			throw new FdfsUploadImageException("upload ThumbImage error", e.getCause());
		} finally {
			IOUtils.closeQuietly(thumbImageStream);
		}
	}

	/**
	 * 是否是支持的图片文件
	 * @param fileExtName
	 * @return
	 */
	private boolean isSupportImage(String fileExtName) {
		return SUPPORT_IMAGE_LIST.contains(fileExtName.toUpperCase());
	}

	/**
	 * 获取byte流
	 * @param inputStream
	 * @return
	 */
	private byte[] inputStreamToByte(InputStream inputStream) {
		try {
			return IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			logger.error("image inputStream to byte error", e);
			throw new FdfsUploadImageException("upload ThumbImage error", e.getCause());
		}
	}

	/**
	 * 检查是否有MateData
	 * @param metaDataSet
	 * @return
	 */
	private boolean hasMateData(Set<MateData> metaDataSet) {
		return null != metaDataSet && !metaDataSet.isEmpty();
	}

	/**
	 * 获取缩略图
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	private ByteArrayInputStream getThumbImageStream(InputStream inputStream, Integer width, Integer height)
			throws IOException {
		// 在内存当中生成缩略图
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if (width == null) {
			width = thumbImageConfig.getWidth();
		}
		if (height == null) {
			height = thumbImageConfig.getHeight();
		}
		// @formatter:off
		Thumbnails.of(inputStream).size(width, height).toOutputStream(out);
		// @formatter:on
		return new ByteArrayInputStream(out.toByteArray());
	}

	/**
	 * 生成缩略图
	 *
	 * @param inputStream
	 * @param thumbImage
	 * @return
	 * @throws IOException
	 */
	private ByteArrayInputStream generateThumbImageStream(InputStream inputStream, ThumbImage thumbImage)
			throws IOException {
		// 根据传入配置生成缩略图
		if (thumbImage.isDefaultConfig()) {
			// 在中间修改配置，这里不是一个很好的实践，如果有时间再进行优化
			thumbImage.setDefaultSize(thumbImageConfig.getWidth(), thumbImageConfig.getHeight());
			return generateThumbImageByDefault(inputStream);
		} else if (thumbImage.getPercent() != 0) {
			return generateThumbImageByPercent(inputStream, thumbImage);
		} else {
			return generateThumbImageBySize(inputStream, thumbImage);
		}

	}

	/**
	 * 根据传入比例生成缩略图
	 *
	 * @param inputStream
	 * @param thumbImage
	 * @return
	 * @throws IOException
	 */
	private ByteArrayInputStream generateThumbImageByPercent(InputStream inputStream, ThumbImage thumbImage)
			throws IOException {
		logger.debug("根据传入比例生成缩略图");
		// 在内存当中生成缩略图
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		// @formatter:off
		Thumbnails.of(inputStream).scale(thumbImage.getPercent()).toOutputStream(out);
		// @formatter:on
		return new ByteArrayInputStream(out.toByteArray());
	}

	/**
	 * 根据传入尺寸生成缩略图
	 *
	 * @param inputStream
	 * @param thumbImage
	 * @return
	 * @throws IOException
	 */
	private ByteArrayInputStream generateThumbImageBySize(InputStream inputStream, ThumbImage thumbImage)
			throws IOException {
		logger.debug("根据传入尺寸生成缩略图");
		// 在内存当中生成缩略图
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		// @formatter:off
		Thumbnails.of(inputStream).size(thumbImage.getWidth(), thumbImage.getHeight()).toOutputStream(out);
		// @formatter:on
		return new ByteArrayInputStream(out.toByteArray());
	}

	/**
	 * 获取缩略图
	 *
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	private ByteArrayInputStream generateThumbImageByDefault(InputStream inputStream) throws IOException {
		logger.debug("根据默认配置生成缩略图");
		// 在内存当中生成缩略图
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		// @formatter:off
		Thumbnails.of(inputStream).size(thumbImageConfig.getWidth(), thumbImageConfig.getHeight()).toOutputStream(out);
		// @formatter:on
		return new ByteArrayInputStream(out.toByteArray());
	}

	/*--------------------------------------------------------------
	 *          getter、setter
	 * -------------------------------------------------------------*/

	public CommandExecutor getCommandExecutor() {
		return commandExecutor;
	}

	public void setCommandExecutor(CommandExecutor commandExecutor) {
		this.commandExecutor = commandExecutor;
	}

	public TrackerClient getTrackerClient() {
		return trackerClient;
	}

	public void setTrackerClient(TrackerClient trackerClient) {
		this.trackerClient = trackerClient;
	}

	public ThumbImageConfig getThumbImageConfig() {
		return thumbImageConfig;
	}

	public void setThumbImageConfig(ThumbImageConfig thumbImageConfig) {
		this.thumbImageConfig = thumbImageConfig;
	}

	@Override
	public StorePath uploadFile(String groupName, byte storeIndex, InputStream inputStream, long fileSize,
			String fileExtName) {
		StorageNode storageNode = trackerClient.getStorageNode(groupName);
		UploadFileCommand command = new UploadFileCommand(storeIndex, inputStream, fileExtName, fileSize, false);
		return commandExecutor.execute(storageNode.getInetSocketAddress(), command);
	}

	@Override
	public StorePath uploadFile(String groupName, byte storeIndex, InputStream inputStream, long fileSize,
			String fileExtName, Set<MateData> metaDataSet) {
		StorageNode storageNode = trackerClient.getStorageNode(groupName);
		UploadFileCommand command = new UploadFileCommand(storeIndex, inputStream, fileExtName, fileSize, false);

		StorePath storePath = commandExecutor.execute(storageNode.getInetSocketAddress(), command);
		if (hasMateData(metaDataSet)) {
			// 参数存储
			SetMetadataCommand cmd = new SetMetadataCommand(storePath.getGroup(), storePath.getPath(), metaDataSet,
					StorageMetadataSetType.STORAGE_SET_METADATA_FLAG_OVERWRITE);
			commandExecutor.execute(storageNode.getInetSocketAddress(), cmd);
		}
		return storePath;
	}

}

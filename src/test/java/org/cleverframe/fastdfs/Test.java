package org.cleverframe.fastdfs;

import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 作者：LiZW <br/>
 * 创建时间：2016/11/19 23:38 <br/>
 */
public class Test {
	/**
	 * 日志记录器
	 */
	private final static Logger logger = LoggerFactory.getLogger(Test.class);

	@org.junit.Test
	public void test01() {
		try {
			File file = new File("F:\\QQ20181022170459.png");
			File outFile = new File("F:\\QQ20181022170459_370x190.png");
			if(!outFile.exists()) {
				outFile.createNewFile();
			}
			Thumbnails.of(new FileInputStream(file)).size(370, 190).toFile(outFile);
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//	        FileInputStream inStream = new FileInputStream(file);  
//	        ImageWrapper imageWrapper = ImageReadHelper.read(inStream);
//	        
//	        BufferedImage bufferImage = Thumbnails.of(imageWrapper.getAsBufferedImages()).size(200, 180).asBufferedImage();
//	        //BufferedImage 转 InputStream
//	        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//	        ImageOutputStream imageOutput = ImageIO.createImageOutputStream(byteArrayOutputStream);
//	        ImageIO.write(bufferImage, "jpeg", imageOutput);
//	        InputStream inputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
//	        System.out.println(inputStream.available());
//	        try {
//        	   OutputStream os = new FileOutputStream(outFile);
//        	   int bytesRead = 0;
//        	   byte[] buffer = new byte[8192];
//        	   while ((bytesRead = inputStream.read(buffer, 0, 8192)) != -1) {
//        	    os.write(buffer, 0, bytesRead);
//        	   }
//        	   os.close();
//        	   inputStream.close();
//        	  } catch (Exception e) {
//        	   e.printStackTrace();
//        	  }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@org.junit.Test
	public void test02(){
		String name="group1/M00/00/8F/CgoKzFrG022ABAJoAAHsbPBcVTw487.jpg";
		//文件压缩
		int i= name.indexOf(".");
		if(i>-1){
			String thumbPath = name.substring(0,i)+"_370x190"+name.substring(i);
			System.out.println(thumbPath);
		}
	}
}
